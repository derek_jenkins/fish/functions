# Defined in - @ line 2
function g2loop
	# To run a parallel operation on a set of nodes:
    if test (printf '%s' $argv[1] | egrep '^\-p$|^\-\-parallel$')
        if test (printf '%s' $argv[2..-1] | egrep '\-\-host\=')
           # To run a parallel operation on a set of non-lexicographical nodes (i.e in differing clusters or non-sequential):
            and begin
                set _remoteHost (pecho $argv[2..-1] | egrep '\-\-host\=' | cut -d= -f2-)
                set _remoteCmd (pecho $argv[2..-1] | egrep -v '\-\-host\=')
                set _wholeCmd (parallel -k echo ssh -T (whoami)@ ::: $_remoteHost ::: "\"echo; echo -n 'Node: '; hostname; $_remoteCmd\"" | sed 's/@ /@/g')
                pecho $_wholeCmd | parallel -k -j+0
            end
            # To run a parallel operation on a set of lexicographical nodes (w/ expectation that they are in the same cluster):
            or begin
                set _parallelLoopCmd (pecho $argv[2..-1])
                parallel -k -j+0 -x echo ssh -T (whoami)@\{\} ::: $_parallelLoopCmd[3](seq -f '%02g' $_parallelLoopCmd[1] $_parallelLoopCmd[2]) ::: "\"echo; echo -n 'Node: ';hostname; $_parallelLoopCmd[4]\"" | parallel -k -j+0
            end
        end
    # To get an SSH login to the servers in sequence:
    else if test (printf '%s' $argv[1] | egrep '^\-l$|^\-\-login$')
        if test (printf '%s' $argv[2..-1] | egrep '\-\-host\=')
            and begin
                set _remoteHost (pecho $argv[2..-1] | egrep '\-\-host\=' | cut -d= -f2-)
                for _node in (pecho $_remoteHost)
                    g2 "$_node"
                end
            end
            or begin
                for _node in (seq -f '%02g' $argv[2] $argv[3])
                    g2 "$argv[4]$_node"
                end
            end
        end
    # If $argv[1] is --host=, assume a sequential login:
    else if test (printf '%s' $argv[1] | egrep '^\-\-host\=')
        and begin
            set _remoteHost (pecho $argv | egrep '\-\-host\=' | cut -d= -f2-)
            for _node in (pecho $_remoteHost)
                g2 "$_node"
            end
        end
    else if test (printf '%s' $argv[1]) -ge 0 ^/dev/null
        # Else, if argv[1] is a (positive) number, run command(s) via loop on a set of sequential nodes:
        and begin
            for _node in (seq -f '%02g' $argv[1] $argv[2])
                g2do "$argv[3]$_node" "echo; echo -n 'Node: '; hostname; $argv[4]"
            end
        end
        # Or, if $argv[1] isn't a number - or any of the above options - exit w/ err message:
        or begin
            echo "This function received a syntax error on the first argument \"$argv[1]\". Please check your syntax and try again."
        end
    end
end
