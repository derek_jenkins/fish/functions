function curl-self-link
    curl-k8s-ep (get-self-link "$argv[1]" (k8s-get "$argv[1]" "$argv[2]"))
end