function pecho --description pecho -- useful for preserving new-lines when creating multi-line vars
    printf "%s\n" $argv
end