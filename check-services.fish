function check-services --Description "Quickly check Loaded and Active status of a systemd service."
	parallel -j+0 -x echo ssh -T (whoami)@\{\} ::: $argv[3](seq -f '%02g' $argv[1] $argv[2]) ::: "\"echo; echo -n 'Node: '; hostname; systemctl status $argv[4] | sed -n '/Loaded/p;/Active/{s/(.*//;p}' \"" | parallel -k -j+0
end