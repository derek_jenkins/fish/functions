function will_it_repo --description Get a display on active git branch if we're in a repo. Useful in your prompt.
    git branch 2>/dev/null >/dev/null
    if test $status = 0
    git branch | awk -F\* '{print $2}' | cut -d" " -f2 | sed '/^$/d'
end