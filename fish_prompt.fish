function fish_prompt
    test $SSH_TTY
    and printf (set_color red)$USER(set_color brwhite)'@'(set_color yellow)(prompt_hostname)' '
    test "$USER" = 'root'
    and echo (set_color red)"#"
    # Main
    echo -n (fish_k8s_prompt)
    echo -n (set_color brblue)'['(set_color brblack)(k8s-getcon)(set_color brblue)']' (set_color br_yellow)(will_it_repo) (set_color brblack)(prompt_pwd; echo "λ ")
end