function g2do --description Execute commands over SSH
	set _node (printf "%s" $argv[1] | awk -F. '{print$1}')
    ssh -o LogLevel=QUIET -t (whoami)@"$_node".some.domain "$argv[2]"
end