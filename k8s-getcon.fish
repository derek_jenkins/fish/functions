function k8s-getcon --description Get k8s context
    kubectl config current-context
end