function k8s-get
    kubectl get "$argv[1]" -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}' | grep "$argv[2]"
end