function aggregate-node-resources-by-label --description Aggregates resource use and limits per user-supplied node label
    parallel -k "kubectl describe node" ::: (kubectl get nodes -l "$argv" -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}') | awk '/Name:/{print "\n"$2}/Total limits may be over/{flag=1;next}/Events:{flag=0}flag' | grep -v '\---'
end
