function get-self-link
    set -l RESOURCE (kubectl get "$argv[1]" -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}' | grep "$argv[2]")
    kubectl get "$argv[1]" "$RESOURCE" -o yaml | awk -F' ' '/selfLink/{print$2}' | grep -vE '^$'
end