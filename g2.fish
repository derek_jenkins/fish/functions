function g2 --description Shortcut for SSH calls
    set _node (printf "%s" $argv[1] | awk -F. '{print$1}')
    ssh -t (whoami)@"$_node".some.domain
end