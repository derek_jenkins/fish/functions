function fish_k8s_prompt
    switch (k8s-getcon)
    # Note: these environments are fictional. Change for your own use.
    case singapore
        echo (set_color red)"☸"
    case nonprod-singapore
        echo (set_color yellow)"☸"
    case finland
        echo (set_color green)"☸"
    case nonprod-finland
        echo (set_color blue)"☸"
    end
end