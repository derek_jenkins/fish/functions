function aggNodeRes --description Aggregates resource use and limits on nodes
    parallel -k "kubectl describe node" ::: (kubectl get nodes -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}') | awk '/Name:/{print "\n"$2}/Total limits may be over/{flag=1;next}/Events:{flag=0}flag' | grep -v '\---'
end