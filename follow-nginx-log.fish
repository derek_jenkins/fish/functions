function follow-nginx-log
    curl-k8s-ep /api/v1/namespaces/default/pods/"$argv[1]"/log\?follow=true\&tailLines=1
end