function k8s-setcon --description Set k8s context
    kubectl config use-context "$argv[1]"
end