function curl-k8s-ep
    set -l SECRET (kubectl get secret | awk '/default-token/{print$1}')
    set -l BEAR_DECODE (echo (kubectl get secret "$SECRET" -o yaml | awk -F' ' '/token:/{print$2}') | base64 -D)
    set -l CACERT_DECODE (echo (kubectl get secret "$SECRET" -o yaml | awk -F' ' '/ca.crt: /{print$2}') | base64 -D)
    set -l EP (kubectl get ep kubernetes | awk '/kubernetes/{print$2}')
    curl --silent -k -H "Authorization: Bearer $BEAR_DECODE" --cacert "$CACERT_DECODE" https://"$EP""$argv[1]"
end