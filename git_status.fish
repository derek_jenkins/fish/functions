function gitStatus --description Fetch git status; useful to add in your shell prompt
    if test (find . -maxdepth 1 -type d -name '.git')
        begin
            git status | grep -q "nothing to commit, working tree clean"
            if test $status = 1
                echo "💬"
            end
        end
    end
end